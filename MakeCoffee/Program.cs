﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CoffeeSharp;

namespace MakeCoffee
{
    class Program
    {
        const string PrefixGeneratedOption = "--prefix-generated";

        static string PrefixGenerated = ".js";

        static void Main(string[] args)
        {
            if (args.Contains(PrefixGeneratedOption))
            {
                PrefixGenerated = ".g.js";
            }

            var folders = args.ToList();
            folders.Remove(PrefixGeneratedOption);

            if (!folders.Any())
            {
                Console.WriteLine("No directories provided, nothing to compile");
                return;
            }

            Console.WriteLine("Making a coffee");
            foreach (var path in folders)
            {
                try
                {
                    CompileDirectory(new DirectoryInfo(path));
                } catch (Exception ex)
                {
                    Console.Error.WriteLine("Error while compiling directory: {0}", path);
                    Console.Error.WriteLine("Error: {0}", ex.Message);
                    Console.Error.WriteLine("Abort");
                    return;
                }
            }
            Console.WriteLine("Coffee is ready");
        }

        private static void CompileDirectory(DirectoryInfo directory)
        {
            Console.WriteLine("Compiling directory: {0}", directory.FullName);
            if (!directory.Exists)
            {
                throw new DirectoryNotFoundException("Could not locate a directory with coffee: " + directory.FullName);
            }

            foreach (var file in directory.GetFiles("*.coffee"))
            {
                CompileCoffeeFile(file);
            }

            foreach (var subdir in directory.GetDirectories())
            {
                CompileDirectory(subdir);
            }
        }

        private static void CompileCoffeeFile(FileInfo coffeeFile)
        {
            Console.WriteLine("Compiling file: {0}", coffeeFile.Name);
            var engine = new CoffeeScriptEngine();
            string javaScript = string.Empty;
            using (var reader = new StreamReader(coffeeFile.OpenRead()))
            {
                javaScript = engine.Compile(reader.ReadToEnd());
            }
            File.WriteAllText(GenerateFileName(coffeeFile.FullName), javaScript);
        }

        private static string GenerateFileName(string oldFileName)
        {
            return oldFileName.Replace(".coffee", PrefixGenerated);
        }
    }
}
