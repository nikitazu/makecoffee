# MakeCoffee #

CoffeeScript recursive directory compiler CLI tool

### What is this repository for? ###

* Allows you to recursively compile a bunch of directories with CoffeeScript files
* For every CoffeeScript file the corresponding JavaScript file is created

### How do I get set up? ###

* Download MakeCoffee from this repository of from nuget.org
* Run MakeCoffee.exe path/to/directory1 path/to/directory2 path/to/directoryN
* To prefix generated JavaScript files use --prefix-generated option:

### Who do I talk to? ###

* Nikita B. Zuev <nikitazu@gmail.com>


### Examples ###


Given directory structure:
Scripts
 foo
  a.coffee
  b.coffee
 c.coffee

MakeCoffee.exe --prefix-generated Scripts

Will result in:
Scripts
 foo
  a.coffee
  a.g.js
  b.coffee
  b.g.js
 c.coffee
 c.g.js

